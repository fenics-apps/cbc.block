Repository moved
================

The *cbc.block* repository has moved to https://github.com/blocknics/cbc.block.
